package fun.mortnon.wj.model.utils;

/**
 * 腾讯问卷工具
 *
 * @author dongfangzan
 * @date 2.8.21 10:29 上午
 */
public class WjUtil {
    /** 腾讯问卷域名 */
    private static final String WJ_HOST = "https://wj.qq.com/";


    /**
     * 从url地址中抽取id
     *
     * @param url 腾讯问卷地址
     * @return    腾讯问卷id
     */
    public static String getIdFromUrl(String url) {
        if (StringUtil.isBlank(url) || !url.contains(WJ_HOST)) {
            return null;
        }

        if (url.endsWith("/")) {
            // 去掉最后一位
            url = url.substring(0, url.length() - 1);
        }

        String[] split = url.split("/");
        // 倒数第二位为id
        return split[split.length - 2];
    }

    /**
     * 从url地址中抽取hash值
     *
     * @param url 腾讯问卷地址
     * @return    腾讯问卷hash值
     */
    public static String getHashFromUrl(String url) {
        if (StringUtil.isBlank(url) || !url.contains(WJ_HOST)) {
            return null;
        }

        if (url.endsWith("/")) {
            // 去掉最后一位
            url = url.substring(0, url.length() - 1);
        }

        String[] split = url.split("/");
        // 倒数第一位为hash
        return split[split.length - 1];
    }

    /**
     * 去掉title上面的html标签
     *
     * @param str 字符串
     * @return    去掉标签后的字符串
     */
    public static String titleTrim(String str) {
        if (StringUtil.isBlank(str)) {
            return str;
        }
        return str.replace("&lt;p&gt;", "")
                .replace("&lt;/p&gt;", "")
                .replace("\n", "");
    }
}
